package org.pelp.mortarboard;

import java.util.ArrayList;
import java.util.List;

public class Patent {
    private String id;
    private String title = "None";
    private String annotation;
    private List<String> inventors;
    private List<String> assignee;
    private String country;
    private String applicationDate;
    private String pdfUrl;
    private final List<Patent> citations = new ArrayList<>();
    private final List<Patent> parents = new ArrayList<>();
    private final List<Patent> applications = new ArrayList<>();

    public Patent(String id) {
        this.setId(id);
    }

    @Override
    public String toString() {
        return String.format("%s - %s", this.getId(), this.getTitle());
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public List<String> getInventors() {
        return inventors;
    }

    public List<String> getAssignee() {
        return assignee;
    }

    public String getCountry() {
        return country;
    }

    public String getApplicationDate() {
        return applicationDate;
    }

    public String getPdfUrl() {
        return pdfUrl;
    }

    public List<Patent> getCitations() {
        return citations;
    }

    public List<Patent> getParents() {
        return parents;
    }

    public List<Patent> getApplications() {
        return applications;
    }

    public String getAnnotation() {
        return annotation;
    }

    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setInventors(List<String> inventors) {
        this.inventors = inventors;
    }

    public void setAssignee(List<String> assignee) {
        this.assignee = assignee;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setApplicationDate(String applicationDate) {
        this.applicationDate = applicationDate;
    }

    public void setPdfUrl(String pdfUrl) {
        this.pdfUrl = pdfUrl;
    }

    public void addCitation(Patent citation) {
        this.citations.add(citation);
    }

    public void addParent(Patent parent) {
        this.parents.add(parent);
    }

    public void addApplications(Patent application) {
        this.applications.add(application);
    }
}
