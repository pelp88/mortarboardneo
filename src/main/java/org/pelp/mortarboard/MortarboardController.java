package org.pelp.mortarboard;

import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.io.IOException;
import java.util.*;
import java.util.logging.Level;

public class MortarboardController {
    final PatentTree patentTree = new PatentTree();
    final Parser parser = new Parser();

    @FXML
    public RadioButton onRequest;

    @FXML
    public RadioButton onList;

    @FXML
    public Label label;

    @FXML
    public ToggleButton doSearch;

    @FXML
    public ProgressBar progressBar;

    @FXML
    public TextField request;

    @FXML
    public TextField pageNumber;

    @FXML
    public TextField recursionDepth;

    @FXML
    public void initialize(){
        this.onRequest.setSelected(true);
        java.util.logging.Logger.getLogger("com.gargoylesoftware").setLevel(Level.OFF);
    }

    @FXML
    public void requestButtonPressed(){
        this.onList.setSelected(false);
        this.label.setText("Запрос:");
    }

    @FXML
    public void listButtonPressed(){
        this.onRequest.setSelected(false);
        this.label.setText("Список:");
    }

    @FXML
    public void onSearchButtonPressed() {
        Service<Void> service = new Service<Void>() {
            @Override
            protected Task<Void> createTask() {
                return new Task<Void>() {
                    @Override
                    protected Void call() throws Exception {
                        doJob();
                        return null;
                    }
                };
            }
        };
        service.start();
    }

    public void doJob() {
        this.doSearch.setSelected(true);
        this.progressBar.setProgress(0);
        var request = this.request.getText();
        var pageQuantity = 1;
        var depth = 2;
        var idQueue = new ArrayDeque<String>();
        try {
            pageQuantity = Integer.parseInt(this.pageNumber.getText());
            depth = Integer.parseInt(this.recursionDepth.getText());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        if (request.length() == 0) {
            System.out.println("No request!");
        } else {
            if (this.onList.isSelected()) {
                idQueue.addAll(List.of(request.replaceAll(" ", "").split("(,)")));
            } else if (this.onRequest.isSelected()) {
                for (var i = 0; i < pageQuantity; i++) {
                    try {
                        this.parser.getPage(request, i);
                        idQueue.addAll(this.parser.parseSearchPageIds());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            if (idQueue.size() == 0) {
                System.out.println("Smth gone wrong while preparing search info, aborting...");
                doSearch.setSelected(false);
            } else {
                for (var i = 0; i < depth; i++) {
                    var current = idQueue.pop();
                    var patent = new Patent(current);
                    System.out.println("Main ID - current patent: " + patent);
                    try {
                        this.appendPatent(patent, true);
                        var citations = patent.getCitations();
                        for (var item : citations) {
                            idQueue.addFirst(item.getId());
                        }
                    } catch (IOException e) {
                        patent.setTitle("Not loaded!");
                        e.printStackTrace();
                    }
                    this.progressBar.setProgress((i / depth) * 100);
                }

                for (var i = 0; i < idQueue.size(); i++)  {
                    var current = idQueue.pop();
                    var patent = new Patent(current);
                    System.out.println("Left patent - current patent: " + patent);
                    try {
                        this.appendPatent(patent, false);
                    } catch (IOException e) {
                        patent.setTitle("Not loaded!");
                        e.printStackTrace();
                    }
                    this.progressBar.setProgress((float) (i / idQueue.size()) * 100);
                }
            }
        }
        System.out.println(patentTree);
    }

    private void appendPatent(Patent patent, boolean flag) throws IOException {
        parser.getPage(patent.getId());
        patent.setTitle(parser.parseTitle());
        patent.setPdfUrl(parser.parsePdfUrl());
        patent.setInventors(parser.parseInventors());
        patent.setCountry(parser.parseCountry());
        patent.setAssignee(parser.parseAssignee());
        patent.setApplicationDate(parser.parseDate());
        patent.setAnnotation(parser.parseAnnotation());
        if (flag) {
            processChildren(parser.parseForwardCitations());
        }
        System.out.println("Filled patent: " + patent);
    }

    private void processChildren(List<String> ids) {
        for (var id : ids){
            var patent = new Patent(id);
            System.out.println("Citation - current patent: " + patent);
            try {
                this.appendPatent(patent, true);
                var citations = parser.parseForwardCitations();
                for (var item : citations){
                    var temp = new Patent(item);
                    patent.addCitation(temp);
                    patentTree.addPatent(temp);
                }
            } catch (IOException e) {
                patent.setTitle("Not loaded!");
                e.printStackTrace();
            }
            patentTree.addPatent(patent);
        }
    }
}