package org.pelp.mortarboard;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Calendar;

public class MortarboardApplication extends Application {
    public String VERSION = "0.1";
    public int YEAR = Calendar.getInstance().get(Calendar.YEAR);

    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(MortarboardApplication.class.getResource("/mainWindow.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 1000, 700);
        stage.setTitle("MortarboardNeo version " + VERSION + ", by pelp88 (2021-" + YEAR + ")");
        stage.setScene(scene);
        stage.setMinWidth(1000);
        stage.setMinHeight(700);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}