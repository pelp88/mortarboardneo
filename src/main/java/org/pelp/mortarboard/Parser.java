package org.pelp.mortarboard;

import com.gargoylesoftware.htmlunit.*;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.*;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

public class Parser {
    Document doc;
    JSONObject jsonResult;
    WebClient webClient = new WebClient(BrowserVersion.CHROME);


    public void getPage(String request, int number) throws IOException, ParseException {
        if (request.length() > 0){
            request = String.join("%2b", request.split(" "));
            String json = this.getSearchPageJson(
                    "https://patents.google.com/xhr/query?url=q%3D"
                    + request + "%26oq%3D" + request + "%26page%3D"
                    + number + "&exp=");
            Object obj = new JSONParser().parse(json);
            jsonResult = (JSONObject) obj;
            System.out.println(jsonResult.toJSONString());
        } else {
            throw new IllegalArgumentException("0-sized string on getPage input!");
        }
    }

    public void getPage(String id) throws IOException {
        if (id.length() > 0){
            String html = this.renderPage(String.format("https://patents.google.com/patent/%s", id));

            this.doc = Jsoup.parse(html);
        } else {
            throw new IllegalArgumentException("0-sized string on getPage input!");
        }
    }

    public String renderPage(String url) throws IOException {
        HtmlPage page = this.webClient.getPage(url);
        return page.asXml();
    }

    private String getSearchPageJson(String url) throws IOException {
        try (InputStream is = new URL(url).openStream()) {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
            return readAll(rd);
        }
    }

    public List<String> parseSearchPageIds() { // работает
        var output = new ArrayList<String>();
        var temp = (JSONObject) jsonResult.get("results");
        temp = (JSONObject) ((JSONArray) temp.get("cluster")).get(0);
        var array = (JSONArray) temp.get("result");
        for (Object o : array) {
            try {
                var elem = (JSONObject) ((JSONObject) o).get("patent");
                output.add((String) elem.get("publication_number"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return output;
    }

    public String parseId() { // работает
        List<Element> items = doc.select("meta[name=\"citation_patent_publication_number\"]");
        try {
            return items.get(0).attr("content").replaceAll("(:)+", "");
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    public String parseTitle() { // работает
        List<Element> items = doc.select("meta[name=\"DC.title\"]");
        try {
            return items.get(0).attr("content");
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    public List<String> parseInventors() { // работает
        var items = doc.select("meta[scheme=\"inventor\"]")
                .stream().map(e -> e.attr("content")).collect(Collectors.toList());
        return items.size() > 0 ? items : null;
    }

    public List<String> parseAssignee() { // работает
        var items = doc.select("meta[scheme=\"assignee\"]")
                .stream().map(e -> e.attr("content")).collect(Collectors.toList());
        return items.size() > 0 ? items : null;
    }

    public String parseCountry() { // работает
        List<Element> items = doc.select("dd[itemprop=\"countryName\"]");
        try {
            return items.get(0).text();
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    public String parseDate() { // работает
        List<Element> items = doc.select("meta[name=\"DC.date\"]");
        return items.get(0).attr("content");
    }

    public String parsePdfUrl() { // работает
        List<Element> items = doc.select("meta[name=\"citation_pdf_url\"]");
        return items.get(0).attr("content");
    }

    public List<String> parseForwardCitations() { // работает
        List<String> output = new ArrayList<>();
        doc.select("tr[itemprop=\"forwardReferencesOrig\"]")
                .forEach(e ->
                        output.add(
                                e.select("span[itemprop=\"publicationNumber\"]").text()));
        return output;
    }

    public List<String> parseBackwardCitations() { // работает
        List<String> output = new ArrayList<>();
        doc.select("tr[itemprop=\"backwardReferencesOrig\"]")
                .forEach(e ->
                        output.add(
                                e.select("span[itemprop=\"publicationNumber\"]").text()));
        return output;
    }

    public Map<String, List<String>> parseApplications() { // работает
        Map<String, List<String>> output = new HashMap<>();
        doc.select("li[itemprop=\"applicationsByYear\"]")
                .forEach(e ->
                        output.put(
                                e.select("span[itemprop=\"year\"]").text(), new ArrayList<>()));
        doc.select("li[itemprop=\"application\"]")
                .forEach(e ->
                        output.get(
                                e.select("span[itemprop=\"filingDate\"]").text().split("(-)+")[0])
                                .add(
                                        e.select("span[itemprop=\"documentId\"]").text()
                                                .split("(/)+")[1]
                                ));
        return output;
    }

    public String parseAnnotation() { // работает
        List<Element> items = doc.select("div.abstract");
        try {
            return items.get(0).text();
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }
}
