package org.pelp.mortarboard;

import javafx.scene.control.TreeItem;

import java.util.HashMap;
import java.util.Map;

public class PatentTree {
    final public Map<String, TreeItem<Patent>> patents = new HashMap<>(0);
    final public TreeItem<Patent> rootItem = new TreeItem<>(new Patent("-1"));

    public void addPatent(Patent patent) {
        if (this.getPatentById(patent.getId()) == null) {
            patents.put(patent.getId(), new TreeItem<>(patent));
        }
    }

    public Patent getPatentById(String id) {
        try {
            return patents.get(id).getValue();
        } catch (Exception ignored) {
            return null;
        }
    }

    public TreeItem<Patent> getRootItem() {
        return rootItem;
    }

    public Integer size() {
        return patents.size();
    }

    @Override
    public String toString() {
        return patents.toString();
    }
}
